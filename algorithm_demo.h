#ifndef ALGORITHM_DEMO_H
#define ALGORITHM_DEMO_H

#include <iostream>
#include <vector>

using namespace std;

//构建表示边的结构体
struct Edge {
    //一条边有 2 个顶点
    int initial;
    int end;
    //边的权值
    int weight;
};

struct Graph{
    vector<int> vertex;
    vector<vector<int>> cost;
    int numVertex,numEdge;
};

//最短路径信息
struct Dist
{
    int index;
    int length;
    int pre;
};

class AlgorithmDemo{
public:
    AlgorithmDemo(){};
    ~AlgorithmDemo(){};
    void printVec(const vector<int>& v);
    //排序算法
    void bubbleSort(vector<int>& v);
    void insertSort(vector<int>& v);
    void selectSort(vector<int>& v);
    void shellSort(vector<int>& v);
    void mergeSort(vector<int>& v, int p, int q);
    void quickSort(vector<int>& v,int p, int q);
    void countingSort(vector<int>& v);
    void radixSort(vector<int>& v);
    void bucketSort(vector<int>& v);
    void stackSort(vector<int>& v);
    //查找算法
    int linearSearch(const vector<int>& v,int val);
    int binarySearch(const vector<int>& v,int begin,int end,int val);
    int insertSearch(const vector<int>& v,int begin,int end,int val);
    int hashSearch(const vector<int>& v,int val);
    //最小生成树算法
    void primMST(const vector<vector<int>>& cost);
    void kruskalMST(const vector<vector<int>>& cost);
    //最短路径算法
    void dijkstraSP(const vector<vector<int>>& cost,int begin);
    void floydSP(vector<vector<int>>& cost);
};


#endif // ALGORITHM_DEMO_H
