#include "algorithm_demo.h"
#include <algorithm>
#include <iterator>

void AlgorithmDemo::printVec(const vector<int>& v){
    for_each(v.begin(),v.end(),[](int a){cout<<a<<" ";});
    cout<<endl;
}

/*冒泡排序
 * 1.从第一个开始两两比较取得最大值或最小值
 * 2.重复n-1次
 * 时间复杂度o(n2)
 * 空间复杂度o(1)
 * 稳定
 */
void AlgorithmDemo::bubbleSort(vector<int>& v){
    int temp;
    for(int i=0;i<v.size()-1;i++){
        for(int j=0;j<v.size()-i-1;j++)
        {
            if(v[j]>v[j+1]){
                temp = v[j];
                v[j]=v[j+1];
                v[j+1]=temp;
            }
        }
    }
}


/*插入排序算法
 * 1.初始状态下，将待排序列的第一个元素看作月是有序的子序列。
 * 2.从第二个元素开始开始，向有序子序列中插入后续元素，重复n-1次
 * 时间复杂度o(n2)
 * 空间复杂度o(1)
 * 稳定
 */
void AlgorithmDemo::insertSort(vector<int>& v){
    int p,value;
    for(int i=1;i<v.size();i++){
        p=i;
        value=v.at(i);
        while(p>0 && v.at(p-1)<v.at(i))p--;
        v.erase(v.begin()+i);
        v.insert(v.begin()+p,value);
    }
}


/*选择排序算法
 * 1.每次从待排序列中查找最大或最小值，查找过程重复n-1次。
 * 2.对于每次找到的最大值或者最小值，通过交换元素位置将他们放在合适的位置即可得到有序序列
 * 时间复杂度o(n2)
 * 空间复杂度o(1)
 * 不稳定
 */
void AlgorithmDemo::selectSort(vector<int>& v){
    int min,temp;
    for(int i=0;i<v.size()-1;i++){
        min = i;
        for(int j=i+1;j<v.size();j++){
            if(v.at(j)<v.at(min))
                min=j;
        }
        if(min!=i){
            temp=v.at(min);
            v.at(min)=v.at(i);
            v.at(i)=temp;
        }
    }
}
/*希尔排序
 * 1.按照一定步长将待排序列划分为多个子序列，使用普通的插入排序对子序列进行排序
 * 2.更改步长重复第一步
 * 3.使用普通插入排序算法对整个序列排序
 * 时间复杂度o(n1.3)
 * 空间复杂度o(1)
 * 不稳定
 */
void AlgorithmDemo::shellSort(vector<int>& v){
    int temp,pos,i;
    int step=1;
    // 计算最大步长
    while(step<v.size()/3)
        step=step*3+1;
    // 对于每个步长，分别进行一次插入排序
    while(step){
        cout<<"step="<<step<<endl;
        for(i=step;i<v.size();i+=step){
            temp=v.at(i);
            pos=i;
            while(pos>step-1 && v.at(pos-step)>=temp)
                pos-=step;
            if(pos!=i)
            {
                v.erase(v.begin()+i);
                v.insert(v.begin()+pos,temp);
            }
        }
        step = (step-1)/3;
    }
}

void merge(vector<int>& v, int p, int mid, int q){
    // 分别保存左右序列
    vector<int> left,right;
    int nleft=mid-p+1;
    int nright=q-mid;
    for(int i=0;i<nleft;i++)
        left.push_back(v.at(p-1+i));
    for(int i=0;i<nright;i++)
        right.push_back(v.at(mid+i));
    // 显示左右序列元素
    cout<<"p:"<<p<<" mid:"<<mid<<" q:"<<q<<endl;
    cout<<"left:";
    for_each(left.begin(),left.end(),[](int a){cout<<a<<" ";});
    cout<<endl;
    cout<<"right:";
    for_each(right.begin(),right.end(),[](int a){cout<<a<<" ";});
    cout<<endl;
    // 对当前左右序列合并排序
    int i=0,j=0,k=p-1;
    // 左右子序列都存在
    while(i<nleft && j<nright)
    {
        if(left.at(i)<=right.at(j))
            v.at(k++)=left.at(i++);
        else
            v.at(k++)=right.at(j++);
    }
    // 仅剩左序列
    while(i<nleft)
        v.at(k++)=left.at(i++);
    // 仅剩右序列
    while(j<nright)
         v.at(k++)=right.at(j++);
}
/*归并排序
 * 1.将待排序列划分为左右两个子序列
 * 2.将左右子序列分别重复第一步，直至子序列不可再分，即仅包含一个元素
 * 3.对所有子序列进行两两合并，并完成排序，最终合并完成的序列即有序
 * 时间复杂度o(nlog2n)
 * 空间复杂度o(n)
 * 稳定
 */
void AlgorithmDemo::mergeSort(vector<int>& v, int p, int q){
    if(v.empty()||p>=q)
        return;
    int mid=(p+q)/2;
    // 显示序列分割过程
//    cout<<"p:"<<p<<" mid:"<<mid<<" q:"<<q<<endl;
    mergeSort(v,p,mid);
    mergeSort(v,mid+1,q);
    // 合并排序
    merge(v,p,mid,q);
}

int partition(vector<int>& v,int p,int q)
{
    int temp=0;
    // 分别表示指向首个元素和倒数第2个元素的指针
    int front=p;
    int back=q-1;
    // 最后一个元素表示分割的中间值
    int pivot=v.at(q);
    while(true){
        // 从前往后查找小于中间值的下标
        while(v.at(front)<pivot)front++;
        // 从后往前查找大于中间值的下标
        while(v.at(back)>pivot)back--;
        // 遍历结束标志
        if(front>=back)
            break;
        else
        {
            //找到后交换位置，然后查找下一个
            temp=v.at(front);
            v.at(front)=v.at(back);
            v.at(back)=temp;
            front++;
            back--;
        }
    }
    // 此时front指向大值，交换使得满足左边小，右边大
    temp=v.at(front);
    v.at(front)=pivot;
    v.at(q)=temp;
    // 返回中间值所在序列中的位置
    return front;
}

/*快速排序
 * 1.待排序列中任选一个元素作为中间元素pivot，将所有比pivot小的元素移动到他的左边，所有比他大的元素移动到他的右边
 * 2.将pivot左右两边的子序列看作待排序列，各自重复第一步。直至所有子序列不可再分，即仅包含一个或0个元素，整个序列就变成了一个有序序列
 * 时间复杂度o(nlog2n)
 * 空间复杂度o(nlog2n)
 * 不稳定
 */
void AlgorithmDemo::quickSort(vector<int>& v,int p, int q){
    int par;
    if(q-p<1)
        return;
    else
    {
        par = partition(v,p,q);
        quickSort(v,p,par-1);
        quickSort(v,par+1,q);
    }
}

/*计数排序
 * 1.创建一个长度为(max-min+1)的计数数组，计数数组的偏移量为序列最小值min
 * 2.将序列元素值作为键值使用计数数组下标表示(下标+偏移=元素值)，计数数组的值为下标对应元素出现的次数。
 * 3.为了使算法稳定，将计数数组从第二个元素开始，每个元素都加上前面所有元素的和，此时，计数数组的值表示的是元素在序列中的排序
 * 4.创建一个输出数组，长度与待排序序列一致
 * 5.从后往前遍历待排序序列，以该元素为索引获取计数数组中存储的值，此值即为序列排序后元素应处的位置，将该元素填入输出数组对应位置(下标=计数数组值-1)，计数数组对应值减一
 * 6.遍历完成，输出数组即为有序序列
 * 时间复杂度o(n+m)
 * 空间复杂度o(n+m)
 * 稳定（步骤3改进后稳定）
 * 基于非比较的排序算法
 * 当待排序序列的最大值和最小值差值特别大时，不适合使用计数排序算法
 * 当待排序序列的值不是整数时，不适合使用计数排序算法
 */
void AlgorithmDemo::countingSort(vector<int>& v){
    int m=v.front();
    int n=v.front();
    for(auto item:v){
        if(item>m)
            m=item;
        if(item<n)
            n=item;
    }
    //1.
    int len=m-n+1;
    vector<int> count(len,0);
    //2.
    for(auto item:v)
        count.at(item-n)++;
    //3.
    for(int i=1;i<len;i++)
        count.at(i)+=count.at(i-1);
    //4.
    vector<int> output;
    output.resize(v.size());
    //5.
    for(auto it=v.rbegin();it!=v.rend();it++){
        output.at(count.at(*it-n)-1)=*it;
        count.at(*it-n)--;
    }
    //6.
    v=output;
}

void countSort(vector<int>& v,int pos)
{
    vector<int> count(10,0);
    for(auto item:v)
        count.at((item/pos)%10)++;
    for(int i=1;i<count.size();i++)
        count.at(i)+=count.at(i-1);
    vector<int> output;
    output.resize(v.size());
    for(auto it=v.rbegin();it!=v.rend();it++)
    {
        output.at(count.at((*it/pos)%10)-1)=*it;
        count.at((*it/pos)%10)--;
    }
    v=output;
}
/*基数排序
 * 1.找到序列的最大值，记录位数。
 * 2.从低位到高位，分别对序列进行基数排序，最终实现排序
 * 时间复杂度o(n+m)
 * 空间复杂度o(n+m)
 * 稳定（取决于计数排序）
 * 适用多位整数排序
 */
void AlgorithmDemo::radixSort(vector<int>& v){
    int max=v.front();
    for(auto item:v){
        if(item>max)
            max=item;
    }
    for(int pos=1;max/pos>0;pos*=10)
        countSort(v,pos);
}

/*桶排序
 * 1.将待排序列按照一定规则化分到多个桶中，桶间元素有序
 * 2.对每个桶中的元素分别进行排序
 * 3.依次遍历所有桶中元素即可得到有序序列
 * 时间复杂度o(n+m+n(logn-logm))
 * 空间复杂度o(n+m)
 * 稳定（取决于桶内部排序算法）
 */
void AlgorithmDemo::bucketSort(vector<int>& v){
    int max=v.front();
    int min=v.front();
    for(auto item:v){
        if(item>max)
            max=item;
        if(item<min)
            min=item;
    }
    // 初始化桶
    int num=4;
    int step=(max-min)/4;
    vector<vector<int>> bucketVec;
    bucketVec.resize(num);
    for(auto item:v)
    {
        if(item<min+step)
            bucketVec.at(0).push_back(item);
        else if(item<min+2*step)
            bucketVec.at(1).push_back(item);
        else if(item<min+3*step)
            bucketVec.at(2).push_back(item);
        else
            bucketVec.at(3).push_back(item);
    }
    // 依次对每个桶中元素排序
    v.clear();
    for(auto itemVec:bucketVec)
    {
        bubbleSort(itemVec);
        for(auto item:itemVec)
            v.push_back(item);
    }
}

void downAdjust(vector<int>& v,int parentIndex,int len){
    int temp=v.at(parentIndex);
    int childIndex=2*parentIndex+1;
    while(childIndex<len){
        if(childIndex+1<len && v.at(childIndex+1)>v.at(childIndex))
            childIndex++;
        if(temp>=v.at(childIndex))
            break;
        v.at(parentIndex)=v.at(childIndex);
        parentIndex=childIndex;
        childIndex=2*parentIndex+1;
    }
    v.at(parentIndex)=temp;
}
/*堆排序
 * 1.构造二叉堆
 * 2.依次取出堆顶元素，对其余元素重构二叉堆，直至取出所有元素
 * 3.取出的元素即有序
 * 时间复杂度o(nlogn)
 * 空间复杂度o(1)
 * 不稳定
 */
void AlgorithmDemo::stackSort(vector<int>& v){
    for(int i=v.size()/2-1;i>=0;i--)
        downAdjust(v,i,v.size());
    for(int i=v.size()-1;i>0;i--)
    {
        int temp=v.at(0);
        v.at(0)=v.at(i);
        v.at(i)=temp;
        downAdjust(v,0,i);
    }
}

/*顺序查找
 * 时间复杂度o(n)
 */
int AlgorithmDemo::linearSearch(const vector<int>& v,int val){
    for(int i=0;i<v.size();i++){
        if(v.at(i)==val)
            return i;
    }
    return -1;
}

/*二分查找
 * 时间复杂度o(logn)
 * 查找的序列必须是有序序列
 */
int AlgorithmDemo::binarySearch(const vector<int>& v,int begin,int end,int val){
    int mid=0;
    if(begin>end)
        return -1;
    mid=(end+begin)/2;
    if(v.at(mid)==val)
        return mid;
    if(val<v.at(mid))
        return binarySearch(v,begin,mid-1,val);
    else
        return binarySearch(v,mid+1,end,val);
}

/*插值查找
 * 时间复杂度o(logn)
 * 二分查找的改进，有序序列均匀的时候优于二分查找
 */
int AlgorithmDemo::insertSearch(const vector<int>& v,int begin,int end,int val){
    int mid=0;
    if(begin>end)
        return -1;
    if(begin==end)
    {
        if(v.at(begin)==val)
            return begin;
        return -1;
    }
    mid=begin+((end-begin)/(v.at(end)-v.at(begin))*(val-v.at(begin)));
    if(v.at(mid)==val)
        return mid;
    if(val<v.at(mid))
        return insertSearch(v,begin,mid-1,val);
    else
        return insertSearch(v,mid+1,end,val);
}

int hashFun(int val){
    return val%50;
}
void creatHash(const vector<int>& v,vector<int>& hashVec){
    int index;
    for(auto item:v){
        index=hashFun(item);
        while(hashVec.at(index%50)!=0)
            index++;
        hashVec.at(index)=item;
    }
}
/*哈希查找
 * 时间复杂度o(1)
 */
int AlgorithmDemo::hashSearch(const vector<int>& v,int val){
    vector<int> hashVec;
    hashVec.resize(50);
    creatHash(v,hashVec);
    int hashAdd=hashFun(val);
    while(hashVec.at(hashAdd)!=val){
        hashAdd=(hashAdd+1)%50;
        if(hashVec.at(hashAdd)==0||hashAdd==hashFun(val))
            return -1;
    }
    return hashAdd;
}

int minKey(vector<int>& key,vector<bool>& visited){
    int size=key.size();
    int min=0,min_index=0;//遍历 key 数组使用，min 记录最小的权值，min_index 记录最小权值关联的顶点
    //遍历 key 数组
    for (int v = 0; v < size; v++) {
        //如果当前顶点为被选择，且对应的权值小于 min 值
        if (visited[v] == false && key[v]!=-1 && (key[v] < min || min==0)) {
            //更新  min 的值并记录该顶点的位置
            min = key[v];
            min_index = v;
        }
    }
    return min_index;
}
/*prim算法
 * 求最小生成树
 * 适合稠密图，时间复杂度0(n²)
 * 1.将连同网中的所有顶点（N个）分为两类（假设为A和B）。初始状态下，所有顶点位于B类
 * 2.选择任一顶点，将其从B类移动到A类
 * 3.从B类的所有顶点出发，找到一条连接着A类的某个顶点且权值最小的边，将此边连接着的B类的顶点移动到A类
 * 4.重复执行步骤3,直到B类中所有顶点为空，恰好可以找到N-1条边
 */
void AlgorithmDemo::primMST(const vector<vector<int>>& cost){
    //key 数组用于记录 B 类顶点到 A 类顶点的权值
    //parent 数组用于记录最小生成树中各个顶点父节点的位置，便于最终生成最小生成树
    //visited 数组用于记录各个顶点属于 A 类还是 B 类
    int size=cost.size();
    vector<int> parent(size,-1);
    vector<int> key(size,-1);
    vector<bool> visited(size,0);
    // 选择 key 数组中第一个顶点，开始寻找最小生成树
    key[0] = 0;  // 该顶点对应的权值设为 0
    parent[0] = -1; // 该顶点没有父节点
    visited[0] = 1;
    for(int i=0;i<size;i++){
        int u=minKey(key,visited);
//        cout<<u<<endl;
        visited.at(u)=true;
        for(int v=0;v<size;v++){
            if(cost.at(u).at(v)!=0 && visited.at(v)==false && (key.at(v)==-1 || cost.at(u).at(v)<key.at(v))){
                    parent.at(v)=u;
                    key.at(v)=cost.at(u).at(v);
            }
        }
//        printVec(parent);
//        printVec(key);
//        for_each(visited.begin(),visited.end(),[](bool a){cout<<a<<" ";});
//        cout<<endl;
    }
    cout<<"元素父节点："<<endl;
    printVec(parent);
    cout<<"对应权值："<<endl;
    printVec(key);
}

void cost2edges(const vector<vector<int>>& cost,vector<Edge>& edges){
    int size=cost.size();
//    cout<<"size:"<<size<<endl;
    for(int i=0;i<size;i++)
    {
        for(int j=i+1;j<size;j++)
        {
            if(cost.at(i).at(j)!=0)
            {
                Edge edge;
                edge.initial=i;
                edge.end=j;
                edge.weight=cost.at(i).at(j);
                edges.push_back(edge);
            }
        }
    }
    cout<<"graph to edges raw:"<<endl;
    for(const auto &item:edges)
        cout<<item.weight<<" ";
    cout<<endl;
}
/*kruskal算法
 * 求最小生成树
 * 适合稀疏图，时间复杂度o(elog(e))
 * 贪心策略
 * 1.将所有边按照权值排序
 * 2.依次取N-1条权值最小的边，组成最小生成树(如果加入时产生回路就跳过这条边，加入下一条边)
 * 判断是否产生回路的方法：
 * 1.初始状态下，为连通网中的各个顶点配置不同的标记
 * 2.对于一个新边，如果它两端顶点的标记不同，就不会构成环路，可以组成最小生成树
 * 3.一旦新边被选择，需要将它的两个顶点以及和它直接相连的所有已选边两端的顶点改为相同的标记
 */
void AlgorithmDemo::kruskalMST(const vector<vector<int>>& cost){
    int N=cost.size();
    vector<Edge> edges;
    cost2edges(cost,edges);
    vector<Edge> minTree(N-1);
    // 对边按照权值排序
    sort(edges.begin(),edges.end(),[&](Edge a,Edge b){return a.weight<b.weight;});
    cout<<"cost of edges sorted :"<<endl;
    for(const auto &item:edges)
        cout<<item.weight<<" ";
    cout<<endl;
    // 初始化顶点标记
    vector<int> assists(N);
    for(int i=0;i<N;i++)
        assists[i]=i;
    int num=0;
    for(const auto &edge:edges){
        int initial=edge.initial;
        int end=edge.end;
        //判断是否构成环
        if(assists[initial]!=assists[end]){
            //将边添加到最小生成树中
            minTree[num++]=edge;
            //将该边的顶点标记修改为相同值
            int elem=assists[end];
            for(auto &item:assists)
                if(item==elem)
                    item=assists[initial];
            //取到N-1条边时，完成最小生成树
            if(num==N-1)
                break;
        }
//        cout<<"current assists:"<<endl;
//        printVec(assists);
    }
    // 输出最小生成树
    cout<<"output edges of MST:"<<endl;
    for(const auto &item:minTree)
        cout<<item.weight<<" ";
    cout<<endl;
}

/*dijkstra算法
 * 单源最短路径
 * 适合有向图和无向图
 * 必须保证权值为非负数
 */
void AlgorithmDemo::dijkstraSP(const vector<vector<int>>& cost,int begin){
    Graph graph;
    graph.vertex={0,1,2,3,4,5,6};
    graph.cost=cost;
    graph.numVertex=cost.size();
    graph.numEdge=9;
    //为各个顶点配置一个标记值，用于确认该顶点是否已经找到最短路径
    vector<bool> final(graph.numVertex,0);
    //初始化路径和对应路径的权值
    vector<int> path(graph.numVertex,begin);
    vector<int> weight(graph.numVertex);
    for(int i=0;i<graph.numVertex;i++)
        weight[i]=graph.cost[begin][i];
    final[begin]=1;
    for(int i=0;i<graph.numVertex;i++){
        int min=-1;
        //选择到各顶点权值最小的顶点，即为本次能确定最短路径的顶点
        int k=0;
        for(int j=0;j<graph.numVertex;j++){
            if(!final[j]){
                if(weight[j]<min||min==-1)
                {
                    k=j;
                    min=weight[j];
                }
            }
        }
        //设置该顶点的标志位为1，避免下次重复判断
        final[k] = 1;
        //对begin到各顶点的权值进行更新
        for(int j=0;j<graph.numVertex;j++){
            if(!final[j] && (min+graph.cost[k][j]<weight[j])){
                weight[j]=min+graph.cost[k][j];
                path[j]=k;//记录各个最短路径上存在的顶点
            }
        }
    }
    cout<<"Shortest path:"<<endl;
    for (int i = 0; i < graph.numVertex; i++) {
        if(i==begin)
            continue;
        cout<<i<<"->"<<begin<<": "<<i<<"-";
        int j = i;
        //由于每一段最短路径上都记录着经过的顶点，所以采用嵌套的方式输出即可得到各个最短路径上的所有顶点
        while (path[j] != begin) {
            cout<<path[j]<<"-";
            j = path[j];
        }
        cout<<begin<<" [weight]: "<<weight[i]<<endl;
    }
}

// 中序递归输出各个顶点之间最短路径的具体线路
void printPath(int i, int j, const vector<vector<int>>& path)
{
    int k = path[i][j];
    if (k == 0)
        return;
    printPath(i, k, path);
    printf("%d-", k);
    printPath(k, j, path);
}
// 输出各个顶点之间的最短路径
void printMatrix(const vector<vector<int>>& cost,const vector<vector<int>>& path) {
    int N=cost.size();
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            if (j == i)
                continue;
            printf("%d->%d: 最短路径为:", i, j);
            if (cost[i][j] >= 1000000)
                printf("%s\n", "INF");
            else {
                printf("%d", cost[i][j]);
                printf("，依次经过：%d-", i);
                //调用递归函数
                printPath(i, j, path);
                printf("%d\n", j);
            }
        }
    }
}
/*floyd算法
 * 多源最短路径
 * 动态规划思想
 * 时间复杂度o(n3)
 */
void AlgorithmDemo::floydSP(vector<vector<int>>& cost){
    int N=cost.size();
    vector<int> temp(N,0);
    vector<vector<int>> path(N,temp);
    for(int k=0;k<N;k++){
        for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
                if(cost[i][k]+cost[k][j]<cost[i][j]){
                    cost[i][j]=cost[i][k]+cost[k][j];
                    //记录路径
                    path[i][j]=k;
                }
            }
        }
    }
    printMatrix(cost,path);
}













