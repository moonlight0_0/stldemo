#include "vector_demo.h"
#include <iostream>


void VectorDemo::printVec(vector<int> &vec){
    for( auto it=vec.begin();it!=vec.end();it++){
        cout<<*it<<" ";
    }
    cout<<endl;
}
