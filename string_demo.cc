#include "string_demo.h"
#include <iostream>

void StringDemo::io_demo(string str){
    int pos = 0;
    while (true)
    {
        int ret = str.find(' ',pos);
        if(ret<0)
        {
            cout<<str.substr(pos,str.size()-pos)<<endl;
            break;
        }
        cout<<str.substr(pos,ret-pos)<<endl;
        pos= ret+1;
    }
    string temp = str.substr(str.find_last_of(' ')+1);
    cout<<"最后一个字符串长度："<<temp.size()<<endl;

    str[0]='t';
    cout<<str<<endl;
    str.at(0)='T';
    cout<<str<<endl;
    temp = '.';
    str.append(temp);
    cout<<str<<endl;
    str.pop_back();
    cout<<str<<endl;
    str.push_back('.');
    str.insert(0,"hello world!  ");
    cout<<str<<endl;
    cout<<str.front()<<endl;
    cout<<str.back()<<endl;
    cout<<"str:"<<str.assign("hello world",6,5)<<endl;
    temp = "hello";
    str.swap(temp);
    cout<<"str+temp:"<<str+temp<<endl;

}
