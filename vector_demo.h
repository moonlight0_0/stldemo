#ifndef VECTOR_DEMO_H
#define VECTOR_DEMO_H

#include <vector>

using namespace std;

class VectorDemo
{
public:
    VectorDemo() {}
    void printVec(vector<int> &vec);
};


#endif // VECTOR_DEMO_H
