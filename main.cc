#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <deque>
#include <queue>
#include <stack>
#include <list>
#include <set>
#include <map>
#include <algorithm>
#include <numeric>
#include <ctime>
#include <iterator>
#include "string_demo.h"
#include "vector_demo.h"
#include "algorithm_demo.h"

using namespace std;

class Person{
    friend void printMap(map<int,Person> m);
private:
    int num;
    string name;
    float score;
public:
    Person(){}
    Person(int num,string name,float score){
        this->num=num;
        this->name=name;
        this->score=score;
    }

};


void stringDemo()
{
    string str = "The following behavior-changing defect reports were applied retroactively to previously published C++ standards";
    istringstream strin(str);
    getline(strin, str);
    StringDemo sd;
    sd.io_demo(str);
}

void vectorDemo()
{
    // 定义并初始化为5个100
    cout<<"vec(5,100)//定义并初始化为5个100"<<endl;
    vector<int> vec(5,100);
    VectorDemo vd;
    vd.printVec(vec);
    // 使用vec初始化vec1
    cout<<"vec1(vec.begin(),vec.end())//使用vec初始化vec1"<<endl;
    vector<int> vec1(vec.begin(),vec.end());
    vd.printVec(vec1);
    // 先定义，后初始化为10个10
    cout<<"vec2.assign(10,10)//先定义，后初始化为10个10"<<endl;
    vector<int> vec2;
    vec2.assign(10,10);
    vd.printVec(vec2);
    // insert 插入3个500
    cout<<"vec2.insert(vec2.begin()+2,3,500)//insert 在begin+2位置插入3个500"<<endl;
    vec2.insert(vec2.begin()+2,3,500);
    vd.printVec(vec2);
    // erase 删除元素begin后1个元素
    cout<<"vec2.erase(vec2.begin(),vec2.begin()+1)//erase 删除元素begin后1个元素"<<endl;
    vec2.erase(vec2.begin(),vec2.begin()+1);
    vd.printVec(vec2);
    vec2.reserve(20);//只改变容量，不改变大小
//    vec2.resize(20);//既改变容量，也改变大小
    vd.printVec(vec2);
    cout<<"大小："<<vec2.size()<<" 容量："<<vec2.capacity()<<endl;
    // swap 收缩空间
    vector<int>(vec2).swap(vec2);
    cout<<"大小："<<vec2.size()<<" 容量："<<vec2.capacity()<<endl;

    vector<vector<int>> v;
    v.push_back(vec);
    v.push_back(vec1);
    v.push_back(vec2);
    // 遍历
    cout<<"迭代器遍历-----------------------"<<endl;
    for(auto it=v.begin();it!=v.end();it++){
        for(auto j=(*it).begin();j!=(*it).end();j++){
            cout<<*j<<" ";
        }
        cout<<endl;
    }
    // 排序
    cout<<"sort 排序算法-----------------------"<<endl;
    sort(vec2.begin(),vec2.end());
    vd.printVec(vec2);
    sort(vec2.begin(),vec2.end(),greater<int>());
    vd.printVec(vec2);
    sort(vec2.begin(),vec2.end(),less<int>());
    vd.printVec(vec2);
    sort(vec2.begin(),vec2.end(),[](int& a,int& b){return a>b;});
    for_each(vec2.begin(),vec2.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

    cout<<"merge---合并两个有序算法--------------------"<<endl;
    vector<int> v1,v2,v3;
    v1.push_back(10);
    v1.push_back(30);
    v1.push_back(50);
    v1.push_back(70);
    v2.push_back(20);
    v2.push_back(40);
    v2.push_back(60);
    v2.push_back(80);
    v3.resize(v1.size()+v2.size());
    merge(v1.begin(),v1.end(),v2.begin(),v2.end(),v3.begin());
    for_each(v3.begin(),v3.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

    cout<<"reverse 反转-------------------"<<endl;
    reverse(v3.begin(),v3.end());
    for_each(v3.begin(),v3.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

    cout<<"random_shuffle 打乱顺序--------------------"<<endl;
    srand(time(NULL));
    random_shuffle(v3.begin(),v3.end());
    for_each(v3.begin(),v3.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

    cout<<"copy 拷贝替换算法----------------------"<<endl;
    copy(v1.begin(),v1.end(),v2.begin());
    copy(v1.begin(),v1.end(),ostream_iterator<int>(cout," "));
    cout<<endl;
    cout<<"replace(v1.begin(),v1.end(),30,300)//30替换为300"<<endl;
    replace(v1.begin(),v1.end(),30,300);
    copy(v1.begin(),v1.end(),ostream_iterator<int>(cout," "));
    cout<<endl;
    cout<<"replace_if(v1.begin(),v1.end(),[](int& a){return a>100;},30)//大于100替换为30"<<endl;
    replace_if(v1.begin(),v1.end(),[](int& a){return a>100;},30);
    copy(v1.begin(),v1.end(),ostream_iterator<int>(cout," "));
    cout<<endl;

    cout<<"swap(v1,v2)//互换两个容器"<<endl;
    v2.clear();
    v2.assign(5,20);
    swap(v1,v2);
    for_each(v1.begin(),v1.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

    cout<<"accumulate(begin,end,val)//累加求和sum+val"<<endl;
    cout<<"sum+0:"<<accumulate(v1.begin(),v1.end(),0)<<endl;

    cout<<"fill(begin,end,val)//使用val填充"<<endl;
    fill(v1.begin(),v1.end(),10);
    for_each(v1.begin(),v1.end(),[](int& a){cout<<a<<" ";});
    cout<<endl;

}

void printDeq(deque<int> deq){
    for(auto it=deq.begin();it!=deq.end();it++)
    {
        cout<<*it<<" ";
    }
    cout<<endl;
}
// 双端队列
void dequeDemo()
{
    deque<int> d1;
    d1.push_back(1);
    d1.push_back(2);
    d1.push_back(3);
    d1.push_front(4);
    d1.push_front(5);
    printDeq(d1);
    cout<<d1.size()<<endl;
    d1.insert(d1.begin(),5,100);
    printDeq(d1);
    d1.pop_front();
    printDeq(d1);
}
// 先进后出，不提供遍历，没有迭代器
void stackDemo(){
    stack<int> s;
    s.push(10);
    s.push(20);
    s.push(30);
    s.push(40);
    s.push(50);
    cout<<s.size()<<endl;
    // 查看元素
    while(!s.empty()){
        cout<<s.top()<<" ";
        s.pop();
    }
    cout<<endl;
}

void queueDemo(){
    // 队列：先进先出，队尾新增，队头移除
    queue<int> q;
    q.push(50);
    q.push(20);
    q.push(10);
    q.push(40);
    q.push(30);
    cout<<"队列长度："<<q.size()<<endl;
    while(!q.empty()){
        cout<<q.front()<<" ";
        q.pop();
    }
    cout<<endl;

    /* 优先队列：priority_queue
     * 可以解决一些贪心问题，也可以对 Dijkstra 算法进行优化。
     * 需要要注意使用 top() 函数之前，必须用 empty() 判断优先队列是否为空。
     */
    // 1.
    priority_queue<int> q1;
    q1.push(3);     //入队
    q1.push(5);
    q1.push(1);
    cout<<"队列长度："<<q1.size()<<endl;
    while(!q1.empty()){
        cout<<q1.top()<<" ";
        q1.pop();
    }
    cout<<endl;
    // 2.等价于1
    // vector<string>填写的是承载底层数据结构堆(heap)的容器，如果是其他类型 可写为 vector<int>或vector<char>;
    // greater<string> 则是对第一个参数的比较类,less<int> 表示数字大的优先级越大，而 greater<int> 表示数字小的优先级越大。
    priority_queue<string,vector<string>,greater<string>> q2;
    q2.push("c");     //入队
    q2.push("z");
    q2.push("a");
    cout<<"队列长度："<<q2.size()<<endl;
    while(!q2.empty()){
        cout<<q2.top()<<" ";
        q2.pop();
    }
    cout<<endl;

}

void printList(list<int> l){
    for(auto it=l.begin();it!=l.end();it++)
    {
        cout<<*it<<" ";
    }
    cout<<endl;
}

void listDemo(){
    list<int> l;
    l.push_back(6);
    l.push_back(1);
    l.push_back(2);
    l.push_back(3);
    l.push_front(4);
    l.push_front(5);
    list<int>::iterator it=l.begin();
    it++;//双向迭代器不支持+2,支持++
    l.insert(it,5,8);
    cout<<l.size()<<endl;
    printList(l);
    // STL算法提供的算法，只支持随即访问迭代器，而list的迭代器是双向迭代器
    // list提供了成员函数可以实现排序
    l.sort();
    printList(l);

}

void printSet(set<int> s){
    for(auto it=s.begin();it!=s.end();it++)
        cout<<*it<<" ";
    cout<<endl;
}

/*字典容器
 * 自动根据键值排序，不支持push pop操作，而需要使用insert
 * set 存放自定义数据必须修改排序规则
 */
void setDemo(){
    // 仿函数：重载函数调用运算符()的类
    cout<<"set<int> s//新建int字典s:";
    set<int> s;
    s.insert(40);
    s.insert(20);
    s.insert(30);
    s.insert(10);
    printSet(s);
    cout<<"s.insert(10)//set键值须唯一因此不执行"<<endl;
    s.insert(10);
    cout<<"s.size()//字典长度:"<<s.size()<<endl;
    // 查找
    set<int>::iterator it;
    cout<<"s.find(10)//查找字典中元素，返回迭代器，取值*it:";
    it = s.find(10);//返回迭代器
    if(it!=s.end())
        cout<<*it<<endl;
    else
        cout<<"没有查找到该元素！"<<endl;
    cout<<"s.count(10)//s中元素计数：";
    cout<<s.count(10)<<endl;
    // 上下限
    cout<<"s.upper_bound(30)//元素上边界迭代器取值*it：";
    it = s.upper_bound(30);
    cout<<*it<<endl;
    cout<<"s.lower_bound(30)//元素下边界迭代器取值*it：";
    it = s.lower_bound(30);
    cout<<*it<<endl;
    pair<set<int>::iterator,set<int>::iterator> p;
    cout<<"s.equal_range(30)//元素上下边界迭代器取值pair<it,it>(*left,*right):";
    p = s.equal_range(30);
    cout<<*(p.first)<<" "<<*(p.second)<<endl;

}

// multiset 与 set 类似，唯一差别是允许键值重复
void multisetDemo(){
    multiset<int> ms;
    ms.insert(40);
    ms.insert(20);
    ms.insert(30);
    ms.insert(10);
    ms.insert(20);
    cout<<"multiset允许键值重复："<<endl;
    for(auto it=ms.begin();it!=ms.end();it++)
        cout<<*it<<" ";
    cout<<endl;
}

void printMap(map<int,Person> m){
    for(auto it=m.begin();it!=m.end();it++)
        cout<<(*it).first<<" "<<(*it).second.name<<" "<<(*it).second.score<<endl;
}
// map与set类似，所有的元素都会根据键值自动排序，且键值唯一，但是元素都必须是 pair<> 类型
void mapDemo(){
    map<int, Person> m;
    m.insert(pair<int,Person>(103,Person(103,"lucy",88.6f)));
    m.insert(make_pair(101,Person(101,"bob",65.3f)));
    m.insert(map<int,Person>::value_type(105,Person(105,"tom",98.3f)));
    m[102]=Person(102,"yueyue",87.5f);
    printMap(m);
}

void algorithmDemo(){
    AlgorithmDemo ad;
    vector<int> v={10,40,56,4,8,95,45,78,63,39,12,52,25,85,102,26};
    cout<<"原始数列："<<endl;
    ad.printVec(v);

    // 排序
    cout<<"--------------排序算法------------"<<endl;
//    ad.bubbleSort(v);
//    ad.insertSort(v);
//    ad.selectSort(v);
//    ad.shellSort(v);
//    ad.mergeSort(v,1,v.size());
//    ad.quickSort(v, 0, v.size()-1);
//    ad.countingSort(v);
//    ad.radixSort(v);
//    ad.bubbleSort(v);
    ad.stackSort(v);
    cout<<"排序后数列："<<endl;
    ad.printVec(v);

    // 查找
    cout<<"--------------查找算法------------"<<endl;
    cout<<"线性查找元素95的位置："<<ad.linearSearch(v,95)<<endl;
    cout<<"二分查找元素95的位置："<<ad.binarySearch(v,0,v.size()-1,95)<<endl;
    cout<<"插值查找元素95的位置："<<ad.insertSearch(v,0,v.size()-1,95)<<endl;
    cout<<"hash查找元素95的位置："<<ad.hashSearch(v,95)<<endl;

    // 最小生成树
    cout<<"--------------最小生成树------------"<<endl;
    vector<vector<int>> cost;
    cost= {{0,6,3,0,7,0},
           {6,0,4,2,0,5},
           {3,4,0,3,8,0},
           {0,2,3,0,0,2},
           {7,0,8,0,0,0},
           {0,5,0,2,0,0}};
    for(const auto &vecItem:cost){
        for(auto item:vecItem)
            cout<<item<<" ";
        cout<<endl;
    }
    ad.primMST(cost);
    ad.kruskalMST(cost);

    // 最短路径
    cout<<"--------------最短路径------------"<<endl;
    cost={{1000000,2,6,1000000,1000000,1000000,1000000},
          {2,1000000,1000000,5,1000000,1000000,1000000},
          {6,1000000,1000000,8,1000000,1000000,1000000},
          {1000000,5,8,1000000,10,15,1000000},
          {1000000,1000000,1000000,10,1000000,6,2},
          {1000000,1000000,1000000,15,6,1000000,6},
          {1000000,1000000,1000000,1000000,2,6,1000000}};
    for(const auto &vecItem:cost){
        for(auto item:vecItem)
            cout<<item<<" ";
        cout<<endl;
    }
    //单源最短路径
    ad.dijkstraSP(cost,1);
    //最短路径
    ad.floydSP(cost);
}

int main(int argc, char *argv[])
{
//    mapDemo();
//    setDemo();
//    multisetDemo();
    algorithmDemo();
//    queueDemo();
    return 0;
}
